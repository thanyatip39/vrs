import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SearchVpKioskRoutes } from './search-vp-kiosk.routing';
import { SearchVpKioskComponent } from './search-vp-kiosk.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchVpKioskRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  declarations: [SearchVpKioskComponent]
})
export class SearchVpKioskModule { }
