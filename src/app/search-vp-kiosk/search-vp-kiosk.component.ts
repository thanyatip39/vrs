import { Component, OnInit, AfterViewInit } from '@angular/core';
declare interface DataTable {
  headerRow: string[];
  dataRows: string[][];
}
export interface Food {
  value: string;
  viewValue: string;
}
export interface Food2 {
  value: string;
  viewValue: string;
}
declare const $: any;

@Component({
  selector: 'app-search-vp-kiosk',
  templateUrl: './search-vp-kiosk.component.html',
  styleUrls: ['./search-vp-kiosk.component.css']
})
export class SearchVpKioskComponent implements OnInit , AfterViewInit {
  isValid = true;
  public dataTable: DataTable;

  foods: Food[] = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' }
  ];

  foods2: Food2[] = [
    { value: 'ddd-2', viewValue: 'wwa' },
    { value: 'sssss-3', viewValue: 'aaaa' },
    { value: 'wwww-4', viewValue: 'dddd' }
  ];
  constructor() { }

  ngOnInit() {

    this.dataTable = {
      headerRow: ['วันที่', 'ต้นทาง', 'ปลายทาง', 'ข้อความ', 'ประเภทเรื่อง', 'ช่องทางการติดตต่อ', 'ผู้ดำเนินการ', 'การจัดการ'],
      dataRows: [
        ['2018-11-07 17:20:03', '0040003 TTRS KIOSK 03 ', '0937936501 คุณแม่ -',
          'โรงเรียนโสตศึกษาจังหวัดสงขลา (ตู้ที่1) คุณดี ใช้บริการ TTRS ติดต่อ คุณแม่ เรื่องไม่รับสาย',
          'สำเร็จ :ไม่รับสาย', 'Kiosk', 'Pajeerat', ''], 
        ['2018-11-26 10:20:45', '5000179 BigC Pattani ', '',
          'กดเล่น',
          'ไม่สำเร็จ :กดเล่น', 'Kiosk', 'Patcharee', ''], 
        ['2018-11-07 17:20:03', '5000159 Hos Chula ', '',
          'กดเล่น *สัญญาณภาพไม่ชัดเจน',
          'ไม่สำเร็จ :กดเล่น', 'Kiosk', 'Patcharee', ''], 
        ['2018-11-07 17:20:03', '5000065 NECTEC - ', '',
          'ไม่มีผู้ใช้บริการ *สัญญาณภาพไม่ชัดเจน',
          'ไม่สำเร็จ :ไม่มีผู้ใช้บริการ', 'Kiosk', 'Patcharee', ''],
          ['2018-11-07 17:20:03', '0040003 TTRS KIOSK 03 ', '0937936501 คุณแม่ -',
          'โรงเรียนโสตศึกษาจังหวัดสงขลา (ตู้ที่1) คุณดี ใช้บริการ TTRS ติดต่อ คุณแม่ เรื่องไม่รับสาย',
          'สำเร็จ :ไม่รับสาย', 'Kiosk', 'Pajeerat', ''], 
        ['2018-11-26 10:20:45', '5000179 BigC Pattani ', '',
          'กดเล่น',
          'ไม่สำเร็จ :กดเล่น', 'Kiosk', 'Patcharee', ''], 
        ['2018-11-07 17:20:03', '5000159 Hos Chula ', '',
          'กดเล่น *สัญญาณภาพไม่ชัดเจน',
          'ไม่สำเร็จ :กดเล่น', 'Kiosk', 'Patcharee', ''], 
        ['2018-11-07 17:20:03', '5000065 NECTEC - ', '',
          'ไม่มีผู้ใช้บริการ *สัญญาณภาพไม่ชัดเจน',
          'ไม่สำเร็จ :ไม่มีผู้ใช้บริการ', 'Kiosk', 'Patcharee', ''],
          ['2018-11-07 17:20:03', '0040003 TTRS KIOSK 03 ', '0937936501 คุณแม่ -',
          'โรงเรียนโสตศึกษาจังหวัดสงขลา (ตู้ที่1) คุณดี ใช้บริการ TTRS ติดต่อ คุณแม่ เรื่องไม่รับสาย',
          'สำเร็จ :ไม่รับสาย', 'Kiosk', 'Pajeerat', ''], 
        ['2018-11-26 10:20:45', '5000179 BigC Pattani ', '',
          'กดเล่น',
          'ไม่สำเร็จ :กดเล่น', 'Kiosk', 'Patcharee', ''], 
        ['2018-11-07 17:20:03', '5000159 Hos Chula ', '',
          'กดเล่น *สัญญาณภาพไม่ชัดเจน',
          'ไม่สำเร็จ :กดเล่น', 'Kiosk', 'Patcharee', ''], 
        ['2018-11-07 17:20:03', '5000065 NECTEC - ', '',
          'ไม่มีผู้ใช้บริการ *สัญญาณภาพไม่ชัดเจน',
          'ไม่สำเร็จ :ไม่มีผู้ใช้บริการ', 'Kiosk', 'Patcharee', ''],
      ]
    };
  }
  ngAfterViewInit() {
    // $('#datatables').DataTable({
    //   'pagingType': 'full_numbers',
    //   'lengthMenu': [
    //     [10, 25, 50, -1],
    //     [10, 25, 50, 'All']
    //   ],
    //   responsive: true,
    //   language: {
    //     search: '_INPUT_',
    //     searchPlaceholder: 'Search records',
    //   }

    // });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function () {
      const $tr = $(this).closest('tr');

      const data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    // Like record

    $('.card .material-datatables label').addClass('form-group');
  }


  changValue(valid: true) {
    this.isValid = valid;
  }

  btnSearch() {

  }
  btnopen() {

  }
  btnedit() {

  }
  btndelete() {

  }
}