import { Routes } from '@angular/router';
import { SearchVpKioskComponent } from './search-vp-kiosk.component';

export const SearchVpKioskRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: SearchVpKioskComponent
    }]
}
];
