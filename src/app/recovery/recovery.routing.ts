import { Routes } from '@angular/router';

import { TechnicalComponent } from './technical/technical.component';
import { VideoPhoneComponent } from './video-phone/video-phone.component';


export const RecoveryRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'video-phone',
        component: VideoPhoneComponent
    }]}, {
    path: '',
    children: [ {
      path: 'technical',
      component: TechnicalComponent
    }]
    }
];
