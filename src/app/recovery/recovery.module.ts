import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';

import { TechnicalComponent } from './technical/technical.component';
import { VideoPhoneComponent } from './video-phone/video-phone.component';
import { RecoveryRoutes } from './recovery.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RecoveryRoutes),
    FormsModule,
    MaterialModule,
  ],
  declarations: [
    TechnicalComponent,
    VideoPhoneComponent
  ]
})

export class RecoveryModule {}
