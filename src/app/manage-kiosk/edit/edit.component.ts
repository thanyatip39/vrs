import { Component, OnInit, AfterViewInit } from '@angular/core';

declare interface DataTable {
  headerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, AfterViewInit {
 public dataTable: DataTable;
  constructor() { }

 

  ngOnInit() {
      this.dataTable = {
          headerRow: [ 'ลำดับ', 'Kiosk_ID', 'ชื่อสถานที่','การจัดการ'],

          dataRows: [
              ['1', 'KIOSK01	', '	พมจ.สตูล',''],
              ['2', 'KIOSK02	', '	พมจ.หนองคาย',''],
              ['3', 'KIOSK03	', '	สมาคมผู้สูงอายุจังหวัดนครพนม',''],
              ['4', 'KIOSK04	', '	มหาวิทยาลัยราชภัฏ จังหวัดสกลนคร',''],
              ['5', 'KIOSK05	', '	ห้างเสริมไทยพลาซา จังหวัดมหาสารคาม',''],
              ['6', 'KIOSK06	', '	บิ๊กซี สาขานครพนม',''],
              ['7', 'KIOSK07	', '	บิ๊กซี สาขาสุรินทร์',''],
              ['8', 'KIOSK08	', '	บิ๊กซี สาขาโคราช',''],
              ['9', 'KIOSK09	', '	ชมรมคนหูหนวกจังหวัดสิงห์บุรี',''],
              ['10', 'KIOSK010	', '	พมจ.สตูล',''],
              ['11', 'KIOSK011	', '	พมจ.สตูล',''],
              ['12', 'KIOSK012	', '	พมจ.สตูล',''],
              ['13', 'KIOSK013	', '	บิ๊กซี สาขานครพนม',''],
              ['14', 'KIOSK014	', '	พมจ.สตูล',''],
              ['15', 'KIOSK015	', '	พมจ.สตูล',''],
              ['16', 'KIOSK016	', '	พมจ.สตูล',''],
              ['17', 'KIOSK017	', 'ชมรมคนหูหนวกจังหวัดสิงห์บุรี',''],
              ['18', 'KIOSK018	', '	พมจ.สตูล',''],
              ['19', 'KIOSK019	', '	พมจ.สตูล',''],
              ['20', 'KIOSK020	', '	บิ๊กซี สาขานครพนม',''],
              ['21', 'KIOSK021	', '	พมจ.สตูล',''],
              ['22', 'KIOSK022	', '	พมจ.สตูล',''],
              ['23', 'KIOSK023	', 'ชมรมคนหูหนวกจังหวัดสิงห์บุรี',''],
              

          ]
       };

  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, -1],
        [10, 25, "All"]
      ],
      responsive: true,
      language: {
        html:"ID Kiosk /ชื่อ Kiosk :",
        search: "_INPUT_",
        searchPlaceholder: "ค้นหา",
      }

    });

    // const table = $('#datatables').DataTable();

    // $('.card .material-datatables label').addClass('form-group');
  }
}