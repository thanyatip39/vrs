import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';


import { ManageKioskRoutes } from './manage-kiosk.routing';
import {CalendarModule} from 'primeng/calendar';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ManageKioskRoutes),
    FormsModule,
    MaterialModule,
    MatCheckboxModule,
    MatInputModule,
    CalendarModule
  ],
  declarations: [
    AddComponent,
    EditComponent
  ]
})

export class ManageKioskModule {}
