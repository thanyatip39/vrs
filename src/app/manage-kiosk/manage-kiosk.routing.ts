import { Routes } from '@angular/router';

import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';


export const ManageKioskRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'edit',
        component: EditComponent
    }]}, {
    path: '',
    children: [ {
      path: 'add',
      component: AddComponent
    }]
    }
];
