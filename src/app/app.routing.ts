import { Routes } from '@angular/router';
// import { } from './queues/queues.module'
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import {AuthGuardService} from './services/auth-guard.service';
export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    }, {
        path: 'callback',
        redirectTo: 'dashboard',
    }, {
        path: '',
        component: AdminLayoutComponent,
        canActivate: [AuthGuardService],
        children: [
            {
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            }, {
                path: 'queues',
                loadChildren: './queues/queues.module#QueuesModule'
            },  {
                path: 'home',
                loadChildren: './home/home.module#HomeModule'
            },  {
                path: 'save-video',
                loadChildren: './save-video/save-video.module#SaveVideoModule'
            },{
                path: 'search-vp-kiosk',
                loadChildren: './search-vp-kiosk/search-vp-kiosk.module#SearchVpKioskModule'
            },{
                path: 'medical',
                loadChildren: './medical/medical.module#MedicalModule'
            },{
                path: 'supervisor',
                loadChildren: './supervisor/supervisor.module#SupervisorModule'
            },{
                path: 'search-supervisor',
                loadChildren: './search-supervisor/search-supervisor.module#SearchSupervisorModule'
            },{
                path: 'technical-issues',
                loadChildren: './technical-issues/technical-issues.module#TechnicalIssuesModule'
            },{
                path: 'statistic',
                loadChildren: './statistic/statistic.module#StatisticModule'
            },{
                path: 'search-technical',
                loadChildren: './search-technical/search-technical.module#SearchTechincalModule'
            },{
                path: 'recovery',
                loadChildren: './recovery/recovery.module#RecoveryModule'
            },{
                path: 'manage-kiosk',
                loadChildren: './manage-kiosk/manage-kiosk.module#ManageKioskModule'
            },{
                path: 'setting',
                loadChildren: './setting/setting.module#SettingModule'
            },
            {
                path: 'components',
                loadChildren: './components/components.module#ComponentsModule'
            }, {
                path: 'forms',
                loadChildren: './forms/forms.module#Forms'
            }, {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            }, {
                path: 'maps',
                loadChildren: './maps/maps.module#MapsModule'
            }, {
                path: 'widgets',
                loadChildren: './widgets/widgets.module#WidgetsModule'
            }, {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            }, {
                path: 'calendar',
                loadChildren: './calendar/calendar.module#CalendarModule'
            }, {
                path: '',
                loadChildren: './userpage/user.module#UserModule'
            }, {
                path: '',
                loadChildren: './timeline/timeline.module#TimelineModule'
            }
        ]
    }, {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: '',
            loadChildren: './pages/pages.module#PagesModule'
        },
        ]
    }, {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: 'pages',
            loadChildren: './pages/pages.module#PagesModule'
        },
        ]
    }
];
