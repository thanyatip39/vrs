import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { QueuesRoutes } from './queues.routing';
import { QueuesComponent } from './queues.component';
import {  ReplyBoxModule } from '../reply-box/reply-box.module';
@NgModule({
  imports: [
    CommonModule,
    ReplyBoxModule,
    RouterModule.forChild(QueuesRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [QueuesComponent]
})
export class QueuesModule { }
