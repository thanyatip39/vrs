import { Routes } from '@angular/router';

import { QueuesComponent } from './queues.component';

export const QueuesRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: QueuesComponent
    }]
}
];
