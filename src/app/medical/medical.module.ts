import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';

import { EmergencyComponent } from './emergency/emergency.component';
import { ImportCaseComponent } from './import-case/import-case.component';
import { ProgressCaseComponent } from './progress-case/progress-case.component';
import { SearchCaseComponent } from './search-case/search-case.component';
import { MedicalRoutes } from './medical.routing';
import {CalendarModule} from 'primeng/calendar';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MedicalRoutes),
    FormsModule,
    MaterialModule,
    MatCheckboxModule,
    MatInputModule,
    CalendarModule
  ],
  declarations: [
    EmergencyComponent,
    ImportCaseComponent,
    ProgressCaseComponent,
    SearchCaseComponent
  ]
})

export class MedicalModule {}
