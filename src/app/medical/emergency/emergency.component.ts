import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-emergency',
  templateUrl: './emergency.component.html',
  styleUrls: ['./emergency.component.css']
})
export class EmergencyComponent implements OnInit {

  cities = [
    {value: '1', viewValue: 'นครราชสีมา'},
    {value: '2', viewValue: 'เชียงใหม่'},
    {value: '3', viewValue: 'กาญจนบุรี'},
    {value: '4', viewValue: 'ตาก'},
    {value: '5', viewValue: 'อุบลราชธานี'},
    {value: '6', viewValue: 'สุราษฎร์ธานี'},
    {value: '7', viewValue: 'ชัยภูมิ'},
  ];

  datas = [
    {value: '1', viewValue: 'TTRS-แจ้งเหตุฉุกเฉิน'},
    {value: '2', viewValue: 'TTRS-การแพทย์ฉุกเฉิน'},
    {value: '3', viewValue: 'TTRS-ครอบครัว-คิดถึง/ห่วงใย'},
    {value: '4', viewValue: 'TTRS-ครอบครัว-นัดหมาย'},
    {value: '5', viewValue: 'TTRS-ครอบครัว-สิ่งของ'},
    {value: '6', viewValue: 'TTRS-ครอบครัว-การเงิน'},
    {value: '7', viewValue: 'TTRS-ครอบครัว-ปรึกษา/เรื่องส่วนตัว'},
  ];

  seasons: string[] = [
    'ปวดท้อง หลัง เชิงกราน และขาหนีบ', 
    'เลือดออก(ไม่มีสาเหตุจากการบาดเจ็บ)', 
    'เจ็บแน่นทรวงอก/หัวใจ/มีปัญหาทางด้านหัวใจ', 
    'ภาวะฉุกเฉินเหตุสิ่งแวดล้อม'
  ];

  seasons2: string[] = [
    'คลุ้มคลั่ง/ภาวะจิตประสาท/อารมณ์', 
    'ชัก/มีสัญญานบอกเหตุการขัก', 
    'ไม่รู้สติ/ไม่ตอบสนอง/หมดสติชั่ววูบ', 
    'ไหม/ลวกเหตุความร้อน/สารเคมี/ไฟฟ้าช๊อต'
  ];

  seasons3: string[] = [
    'หายใจลำบาก/ติดขัด', 
    'สำลักอุดทางเดินหายใจ', 
    'อื่นๆ (เว้นว่าง)', 
    'พิษ/รับยาเกินขนาด'
  ];

  private passport:String;
  isValid: boolean = false;
  
  constructor() { }

  ngOnInit() {
    this.passport='';
  }

  changValue(valid: boolean){
    this.isValid = valid;
  }

}
