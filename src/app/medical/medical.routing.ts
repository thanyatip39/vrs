import { Routes } from '@angular/router';

import { EmergencyComponent } from './emergency/emergency.component';
import { ImportCaseComponent } from './import-case/import-case.component';
import { ProgressCaseComponent } from './progress-case/progress-case.component';
import { SearchCaseComponent } from './search-case/search-case.component';


export const MedicalRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'emergency',
        component: EmergencyComponent
    }]}, {
    path: '',
    children: [ {
      path: 'import-case',
      component: ImportCaseComponent
    }]
    }, {
      path: '',
      children: [ {
        path: 'progress-case',
        component: ProgressCaseComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'search-case',
            component: SearchCaseComponent
        }]
    }
];
