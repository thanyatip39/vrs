import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SearchSupervisorRoutes } from './search-supervisor.routing';
import { SearchSupervisorComponent } from './search-supervisor.component';
import {MatExpansionModule} from '@angular/material/expansion';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchSupervisorRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatExpansionModule
  ],
  declarations: [SearchSupervisorComponent]
})
export class SearchSupervisorModule { }
