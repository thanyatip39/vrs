import { Routes } from '@angular/router';

import { SearchSupervisorComponent } from './search-supervisor.component';

export const SearchSupervisorRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: SearchSupervisorComponent
    }]
}
];
