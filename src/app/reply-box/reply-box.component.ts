import { Component, EventEmitter, OnInit, Output, ViewChild, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { IcrmReplyService } from '../services/icrm-reply.service';
import { IcrmWsService } from '../services/icrm-ws.service';
import { HttpClient, HttpRequest, HttpResponse, HttpEvent } from '@angular/common/http';
import { Subscription } from 'rxjs/'
@Component({
  selector: 'app-reply-box',
  templateUrl: './reply-box.component.html',
  styleUrls: ['./reply-box.component.css'],
})
export class ReplyBoxComponent implements OnInit {
  @Input() dataReply;
  @ViewChild('fileInput') fileInput;
  @Output() editFormFormChild = new EventEmitter()
  checkChange = true;
  replyForm: FormGroup;
  myFormData: FormData;
  httpEvent: HttpEvent<{}>
  tempEvent: HttpEvent<{}>
  accept = '*'
  files: File[] = [];
  webrtc: File[] = [];
  progress: number;
  hasBaseDropZoneOver = false;
  lastFileAt: Date;
  sendableFormData = new FormData();
  httpEmitter: Subscription
  maxSize = 102400000
  @ViewChild('recVideo') recVideo: any;

  @Input() constrains = {video: true, audio: false};
  @Input() fileName = 'my_recording';
  @Input() showVideoPlayer = true;

  @Output() startRecording = new EventEmitter();
  @Output() downloadRecording = new EventEmitter();
  @Output() fetchRecording = new EventEmitter();
  @Output() recorded = null;

  format = 'video/webm';
  _navigator = <any> navigator;
  localStream;
  video;
  mediaRecorder;
  recordedBlobs = null;
  hideStopBtn = true;



  constructor(private formBuilder: FormBuilder, private wsService: IcrmWsService, private replyService: IcrmReplyService) { }

  ngOnInit() {
    this.createForm();
    if (this.recVideo) {
      this.video = this.recVideo.nativeElement;
    }
    this._navigator.getUserMedia = ( this._navigator.getUserMedia || this._navigator.webkitGetUserMedia
      || this._navigator.mozGetUserMedia || this._navigator.msGetUserMedia );
  }

  private _initStream(constrains, navigator) {
    return navigator.mediaDevices.getUserMedia(constrains)
      .then((stream) => {
        this.localStream = stream;
        return window.URL ? window.URL.createObjectURL(stream) : stream;
      })
      .catch(err => err);
  }
  private _stopStream() {
    const tracks = this.localStream.getTracks();
    tracks.forEach((track) => {
      track.stop();
    });
  }

  public start() {
    console.log('start recording');
    this.recordedBlobs = [];
    this._initStream(this.constrains, this._navigator)
      .then((stream) => {
        if (!window['MediaRecorder'].isTypeSupported(this.format)) {
          console.log(this.format + ' is not Supported');
          return;
        }
        try {
          this.mediaRecorder = new window['MediaRecorder'](this.localStream, {mimeType: this.format});
          if (this.video) {
            this.video.src = stream;
          }
          this.startRecording.emit(stream);
        } catch (e) {
          console.error('Exception while creating MediaRecorder: ' + e);
          return;
        }
        console.log('Created MediaRecorder', this.mediaRecorder, 'with options', this.format);
        this.hideStopBtn = false;
        this.mediaRecorder.ondataavailable =
          (event) => {
            if (event.data && event.data.size > 0) {
              this.recordedBlobs.push(event.data);
            }};
        this.mediaRecorder.start(10); // collect 10ms of data
      });
  }

  public stop() {
    console.log('stop recording');
    this.hideStopBtn = true;

    this._stopStream();
    this.mediaRecorder.stop();
    this.fetchRecording.emit(this.recordedBlobs);
    if (this.video) {
      this.video.controls = true;
    }
    const blobfile = new Blob(this.recordedBlobs, {type: this.format});
    this.webrtc = [new File(this.recordedBlobs, this.fileName, blobfile)]
    this.sendableFormData.append('media', this.webrtc[0], this.webrtc[0].name);
  }

  public play() {
    if (!this.video) {
      return;
    }
    const superBuffer = new Blob(this.recordedBlobs, {type: this.format});
    this.video.src = window.URL.createObjectURL(superBuffer);
  }

  public download() {
    console.log('download recorded stream');
    const timestamp = new Date().getUTCMilliseconds();
    const blob = new Blob(this.recordedBlobs, {type: this.format});
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    a.download = timestamp + '__' + this.fileName + '.webm';
    document.body.appendChild(a);
    a.click();
    setTimeout(() => {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
      this.downloadRecording.emit();
    }, 100);
  }


  createForm() {
    this.replyForm = this.formBuilder.group({
      messageReply : ['', Validators.required]
    })
    console.log(typeof this.dataReply['phonenumber']);
  }


  getDate() {
    return new Date()
  }

  replyMessage() {
   this.sendableFormData.append('phonenumber', this.dataReply['phonenumber']);
   this.sendableFormData.append('identification_patient', this.dataReply['identification']);
   this.sendableFormData.append('message_orgtext', this.replyForm.get('messageReply').value);
   this.sendableFormData.append('adduser', this.dataReply['agent']);
   this.sendableFormData.append('message_read_unread_id', '0');
   this.sendableFormData.append('massage_view', '1');
   this.sendableFormData.append('reference_messageid', this.dataReply['messageid']);
    this.replyService.reply(this.sendableFormData)
    .subscribe(event => {
        this.httpEvent = event
        if (event instanceof HttpResponse) {
          delete this.httpEmitter
          console.log('request done', event)
        }
        this.wsService.send({
          event: 'getTimeline',
          agent: this.dataReply['agent'],
          phonenumber: this.dataReply['phonenumber']
        });
      },
      error => console.log('Error Uploading', error))
      this.files.length = 0;
      this.webrtc.length = 0;
      this.replyForm.get('messageReply').setValue('');
  }

  cancelBooking() {
    console.log('cancelBooking');
    this.checkChange = false;
    this.editFormFormChild.emit(false)
    this.wsService.send({
      event: 'cancelBooking',
      agent: this.dataReply['agent'],
      messageid: this.dataReply['messageid']
    });
  }

  cancel() {
    this.progress = 0
    if ( this.httpEmitter ) {
      console.log('cancelled')
      this.httpEmitter.unsubscribe()
    }
  }
}
