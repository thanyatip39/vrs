import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReplyBoxComponent } from './reply-box.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ngfModule, ngf } from 'angular-file';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppComponent } from '../app.component';
@NgModule({
    imports: [ RouterModule,
        MaterialModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ngfModule,
    ],
    declarations: [ ReplyBoxComponent ],
    exports: [ ReplyBoxComponent ]
})

export class ReplyBoxModule {}
platformBrowserDynamic().bootstrapModule(ReplyBoxModule);
