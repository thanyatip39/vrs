import { Routes } from '@angular/router';

import { TechnicalIssuesComponent } from './technical-issues.component';

export const TechnicalIssuesRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: TechnicalIssuesComponent
    }]
}
];
