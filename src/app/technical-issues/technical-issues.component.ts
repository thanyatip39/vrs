import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-technical-issues',
  templateUrl: './technical-issues.component.html',
  styleUrls: ['./technical-issues.component.css']
})
export class TechnicalIssuesComponent implements OnInit {
  ways = [
    {value: '0', viewValue: 'เลือกช่องทาง'},
    {value: '1', viewValue: 'VRS Video Phone'},
    {value: '2', viewValue: 'VRS Mobile'},
    {value: '3', viewValue: 'VRS Mobile'},
    {value: '4', viewValue: 'VRS Kiosk'},
    {value: '5', viewValue: 'VRS Web'},
  ];

  issues = [
    {value: '0', viewValue: 'เลือกหมวดหมู่ปัญหา'},
    {value: '1', viewValue: 'ปัญหาจากอินเทอร์เน็ต-คนหูหนวก'},
    {value: '2', viewValue: 'ปัญหาจากอินเทอร์เน็ต - TTRS'},
    {value: '3', viewValue: 'จอดำ'},
    {value: '4', viewValue: 'Invalid Token'},
    {value: '5', viewValue: 'บันทึก Camtasia ไม่สำเร็จ'},
  ];

  constructor() { }

  ngOnInit() {
  }

}
