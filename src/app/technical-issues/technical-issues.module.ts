import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { TechnicalIssuesRoutes } from './technical-issues.routing';
import { TechnicalIssuesComponent } from './technical-issues.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TechnicalIssuesRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [TechnicalIssuesComponent]
})
export class TechnicalIssuesModule { }
