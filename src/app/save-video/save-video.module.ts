import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SaveVideoRoutes } from './save-video.routing';
import { SaveVideoComponent } from './save-video.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SaveVideoRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [SaveVideoComponent]
})
export class SaveVideoModule { }
