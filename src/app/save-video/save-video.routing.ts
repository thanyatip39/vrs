import { Routes } from '@angular/router';

import { SaveVideoComponent } from './save-video.component';

export const SaveVideoRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: SaveVideoComponent
    }]
}
];
