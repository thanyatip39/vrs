import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';

import { BasicComponent } from './basic/basic.component';
import { PersonalComponent } from './personal/personal.component';
import { SettingRoutes } from './setting.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SettingRoutes),
    FormsModule,
    MaterialModule,
  ],
  declarations: [
    BasicComponent,
    PersonalComponent
  ]
})

export class SettingModule {}
