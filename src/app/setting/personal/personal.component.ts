import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {
  private name:String;
  private email:String;
  private operation:String;
  private extension_in:String;
  private extension_out:String;

  constructor() { }

  ngOnInit() {
    this.name = 'Admin Admin ( AGENT )';
    this.email = 'nattapong120@gmail.com';
    this.operation = ' อ่าน    แก้ไข    ลบ  ';
    this.extension_in = '00025';
    this.extension_out = '00025';
  }

}
