import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css']
})
export class BasicComponent implements OnInit {
  marked = false;

  private api:String;
  private key:String;
  private vrs:String;
  private queues_number:number;
  private making_call:String;
  private receiving:String;
  private emergency:String;
  
  code_video = 'video3';
  Resolution = 'Resolution4';
  frame = 'frame3';
  bit ='bit1';
  code_audio = 'code_audio1';
  sample = 'sample1';

  constructor() { }

  ngOnInit() {
    this.api = 'http://api.ttrs.caas.in.th/api.php';
    this.key = 'b8enAeS6IOV8mWdc';
    this.vrs = 'http://vrs.caas.in.th/vrsapi/';
    this.queues_number = 25;
    this.making_call = '810,811,813,001,002,003,210,211,814,815,850';
    this.receiving = '812,004';
    this.emergency = '1669=การแพทย์ฉุกเฉิน 191=ฉุกเฉินตำรวจ 199=ฉุกเฉินดับเพลิง';
    
  }

  toggleVisibility(e){
    this.marked= e.target.checked;
  }
}
