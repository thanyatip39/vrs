import { Routes } from '@angular/router';

import { BasicComponent } from './basic/basic.component';
import { PersonalComponent } from './personal/personal.component';


export const SettingRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'basic',
        component: BasicComponent
    }]}, {
    path: '',
    children: [ {
      path: 'personal',
      component: PersonalComponent
    }]
    }
];
