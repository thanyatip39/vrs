import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

// Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

// Menu Items
export const ROUTES: RouteInfo[] = [{
    path: '/dashboard',
    title: 'Dashboard',
    type: 'link',
    icontype: 'dashboard'
},
//  {
//     path: '/queues',
//     title: 'Queues',
//     type: 'link',
//     icontype: 'queues'
// },
{
    path: '/home',
    title: 'หน้าแรก',
    type: 'link',
    icontype: 'home'
},{
    path: '/save-video',
    title: 'บันทึกข้อมูล VP / KIOSK',
    type: 'link',
    icontype: 'videocam'
},{
    path: '/search-vp-kiosk',
    title: 'ค้นหาข้อมูล VP / KIOSK',
    type: 'link',
    icontype: 'ondemand_video'
},{
    path: '/medical',
    title: 'การแพทย์ฉุกเฉิน',
    type: 'sub',
    icontype: 'local_hospital',
    collapse: 'medical',
    children: [
        { path: 'emergency', title: 'แจ้งเรื่องการแพทย์ฉุกเฉิน', ab: 'A' },
        { path: 'import-case', title: 'Case ที่นำเข้าระบบ', ab: 'AS' },
        { path: 'progress-case', title: 'Case ที่กำลังดำเนินงาน', ab: 'AP' },
        { path: 'search-case', title: 'ค้นหาข้อมูล Case', ab: 'AA' }
    ]
},{
    path: '/supervisor',
    title: 'สรุปงาน SUPERVISOR',
    type: 'link',
    icontype: 'supervisor_account'
},{
    path: '/search-supervisor',
    title: 'ค้นหาสรุปงาน SUPERVISOR',
    type: 'link',
    icontype: 'search'
},{
    path: '/technical-issues',
    title: 'บันทึกปัญหาทางเทคนิค',
    type: 'link',
    icontype: 'description'
},{
    path: '/search-technical',
    title: 'ค้นหาปัญหาทางเทคนิค',
    type: 'link',
    icontype: 'library_books'
},{
    path: '/statistic',
    title: 'สถิติรายงานประจำวัน',
    type: 'sub',
    icontype: 'equalizer',
    collapse: 'statistic',
    children: [
        { path: 'vrs', title: 'รายงานนำเข้าข้อมูล VRS', ab: 'VR' }
    ]
},{
    path: '/recovery',
    title: 'ข้อมูลที่ถูกลบ',
    type: 'sub',
    icontype: 'delete',
    collapse: 'recovery',
    children: [
        { path: 'video-phone', title: 'ข้อมูล Vedio Phone', ab: 'VR' },
        { path: 'technical', title: 'ข้อมูลปัญญาทางเทคนิค', ab: 'VR' }
    ]
},{
    path: '/manage-kiosk',
    title: 'จัดการตู้ Kiosk',
    type: 'sub',
    icontype: 'phonelink_setup',
    collapse: 'manage-kiosk',
    children: [
        { path: 'edit', title: 'แก้ไขตู้ Kiosk', ab: 'ED' },
        { path: 'add', title: 'เพิ่มตู้ Kiosk', ab: 'AD' }
    ]
},{
    path: '/setting',
    title: 'ตั้งค่า',
    type: 'sub',
    icontype: 'build',
    collapse: 'setting',
    children: [
        { path: 'basic', title: 'ข้อมูลพื้นฐาน', ab: 'VR' },
        { path: 'personal', title: 'ข้อมูลส่วนตัว', ab: 'VR' }
    ]
},
// {
//     path: '/components',
//     title: 'Components',
//     type: 'sub',
//     icontype: 'apps',
//     collapse: 'components',
//     children: [
//         { path: 'buttons', title: 'Buttons', ab: 'B' },
//         { path: 'grid', title: 'Grid System', ab: 'GS' },
//         { path: 'panels', title: 'Panels', ab: 'P' },
//         { path: 'sweet-alert', title: 'Sweet Alert', ab: 'SA' },
//         { path: 'notifications', title: 'Notifications', ab: 'N' },
//         { path: 'icons', title: 'Icons', ab: 'I' },
//         { path: 'typography', title: 'Typography', ab: 'T' }
//     ]
// }, {
//     path: '/forms',
//     title: 'Forms',
//     type: 'sub',
//     icontype: 'content_paste',
//     collapse: 'forms',
//     children: [
//         { path: 'regular', title: 'Regular Forms', ab: 'RF' },
//         { path: 'extended', title: 'Extended Forms', ab: 'EF' },
//         { path: 'validation', title: 'Validation Forms', ab: 'VF' },
//         { path: 'wizard', title: 'Wizard', ab: 'W' }
//     ]
// }, {
//     path: '/tables',
//     title: 'Tables',
//     type: 'sub',
//     icontype: 'grid_on',
//     collapse: 'tables',
//     children: [
//         { path: 'regular', title: 'Regular Tables', ab: 'RT' },
//         { path: 'extended', title: 'Extended Tables', ab: 'ET' },
//         { path: 'datatables.net', title: 'Datatables.net', ab: 'DT' }
//     ]
// }, {
//     path: '/maps',
//     title: 'Maps',
//     type: 'sub',
//     icontype: 'place',
//     collapse: 'maps',
//     children: [
//         { path: 'google', title: 'Google Maps', ab: 'GM' },
//         { path: 'fullscreen', title: 'Full Screen Map', ab: 'FSM' },
//         { path: 'vector', title: 'Vector Map', ab: 'VM' }
//     ]
// }, {
//     path: '/widgets',
//     title: 'Widgets',
//     type: 'link',
//     icontype: 'widgets'

// }, {
//     path: '/charts',
//     title: 'Charts',
//     type: 'link',
//     icontype: 'timeline'

// }, {
//     path: '/calendar',
//     title: 'Calendar',
//     type: 'link',
//     icontype: 'date_range'
// }, {
//     path: '/pages',
//     title: 'Pages',
//     type: 'sub',
//     icontype: 'image',
//     collapse: 'pages',
//     children: [
//         { path: 'pricing', title: 'Pricing', ab: 'P' },
//         { path: 'timeline', title: 'Timeline Page', ab: 'TP' },
//         { path: 'login', title: 'Login Page', ab: 'LP' },
//         { path: 'register', title: 'Register Page', ab: 'RP' },
//         { path: 'lock', title: 'Lock Screen Page', ab: 'LSP' },
//         { path: 'user', title: 'User Page', ab: 'UP' }
//     ]
// },
{
    path: '/logout',
    title: 'Logout',
    type: 'link',
    icontype: 'lock'
}];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    updatePS(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            const ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
}
