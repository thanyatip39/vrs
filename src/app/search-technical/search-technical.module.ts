import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SearchTechincalRoutes } from './search-technical.routing';
import { SearchTechnicalComponent } from './search-technical.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchTechincalRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  declarations: [SearchTechnicalComponent]
})
export class SearchTechincalModule { }
