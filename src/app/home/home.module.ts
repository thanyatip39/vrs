import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { HomeRoutes } from './home.routing';
import { HomeComponent } from './home.component';
import {MatCardModule} from '@angular/material/card';
 
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(HomeRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatCardModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
