import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from 'environments/environment';
import 'rxjs/add/operator/map';



@Injectable()
export class TtrsApiService {


  constructor(private http: Http) {
    // this.getMessageType();
    // this.getTypeComplete();
  }

  public getMessageType() {
    return this.http.get(environment.URL_GET_MESSAGETYPE)
      .map(res => res.json());
  }

  public getNameUser(identification) {
    return this.http.get(environment.URL_GET_NAMEUSER + identification)
      .map(res => res.json());
  }

}
