import { Injectable } from '@angular/core';
import { environment } from 'environments/environment'
import { HttpClient, HttpRequest, HttpResponse, HttpEvent } from '@angular/common/http'

const API_URL = environment.URL_REPLY_MESSAGE;
@Injectable()
export class IcrmReplyService {


  constructor(public http: HttpClient) {}

  reply(fileFormdata: FormData): any {
    const req = new HttpRequest<FormData>('POST', API_URL, fileFormdata, {
      reportProgress: true
    })
    console.log(fileFormdata.get('phonenumber'));
    return this.http.request(req)
  }

}
