import { Injectable } from '@angular/core';
import { Observable, Subject, ReplaySubject } from 'rxjs/';
import { environment  } from 'environments/environment'

const WS_SERVER = environment.WS_SERVER;
export class Subscribable<T> {

  private valueSource: Subject<T> = new ReplaySubject<T>(1);
  public value: Observable<T>;
  private _value: T;
  constructor() {
      this.value = this.valueSource.asObservable();
  }

  public set(val: T) {
      this.valueSource.next(val);
      this._value = val;
  }

  public get(): T {
      return this._value;
  }

  public send(data) {
      this.valueSource.next(data);
  }

  public Create(data) {

  }
}

export class MessageData {
  event: string
  value: Object
}



@Injectable()
export class IcrmWsService {


  // public variable
  public isNodeConnected = false; // use for all
  private subject: Subscribable<MessageData>;
  private websocket: any;

  constructor() {
      this.connect();
  }

  public connect(): Subscribable<MessageData> {
      if (!this.isNodeConnected) {
          this.subject = this.create(WS_SERVER);
        //   this.subject.value.subscribe((msg) => {
        //     // setTimeout(this.connect(), 36000);
        //   })
      }
      return this.subject;
  }

  private create(url): Subscribable<MessageData> {
      this.websocket = new WebSocket(url);
      const subject = new Subscribable<MessageData>();

      this.websocket.onopen = (msg) => {
          console.log('onopen: ', msg)
          this.isNodeConnected = true;
          this.send({
            event:  'update',
          });
          const serviceOpen = {event: 'openService', value: 'service open'};
          subject.set(serviceOpen);

          console.log('Successfully connected: ' + url, this.websocket);
      }

      this.websocket.onmessage = (msg) => {
          console.log('onmessage')
        subject.set(msg.data)

      }

      this.websocket.onclose = (msg) => {
            console.log('onclose')
            this.isNodeConnected = false;
            // subject.send({err: 'service close'})
            const closeServiceMessage = {event: 'err', value: 'service close'};
            subject.set(closeServiceMessage);
            this.connect();
      }

      return subject
  }

  public getSubject() { return this.subject.value; }


  public send(data) {
      console.log('isNodeConnected', this.isNodeConnected);
      if (!this.isNodeConnected) {
          this.connect();
      }


      console.log('this.websocket.readyState', this.websocket.readyState);
      if (this.websocket.readyState === WebSocket.OPEN) {
        this.websocket.send(JSON.stringify(data));
      }

  }


}
