import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import {MatTableModule} from '@angular/material/table';


import { VrsComponent } from './vrs/vrs.component';
import { StatisticRoutes } from './statistic.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(StatisticRoutes),
    FormsModule,
    MaterialModule,
    MatTableModule
  ],
  declarations: [
    VrsComponent
  ]
})

export class StatisticModule {}
