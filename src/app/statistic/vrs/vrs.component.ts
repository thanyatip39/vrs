import { Component, OnInit, AfterViewInit } from '@angular/core';

declare interface DataTable {
  headerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-vrs',
  templateUrl: './vrs.component.html',
  styleUrls: ['./vrs.component.css']
})
export class VrsComponent implements OnInit, AfterViewInit {
  public dataTable: DataTable;

  ngOnInit() {
      this.dataTable = {
          headerRow: [ 'วันที่', 'ผู้สร้าง', 'ไฟล์รายงาน'],

          dataRows: [
              ['2018-11-06', 'auto', ''],
              ['2018-11-05', 'auto', ''],
              ['2018-11-04', 'auto', ''],
              ['2018-11-03', 'auto', ''],
              ['2018-11-02', 'auto', ''],
              ['2018-11-01', 'auto', ''],
              ['2018-10-31', 'auto', ''],
              ['2018-10-30', 'auto', ''],
              ['2018-10-29', 'auto', ''],
              ['2018-10-27', 'auto', ''],
              ['2018-10-26', 'auto', ''],
              ['2018-10-25', 'auto', ''],
              ['2018-10-24', 'auto', ''],
              ['2018-10-23', 'auto', ''],
              ['2018-10-22', 'auto', ''],
              ['2018-10-21', 'auto', ''],
              ['2018-10-20', 'auto', ''],
              ['2018-10-19', 'auto', ''],
              ['2018-10-18', 'auto', ''],
              ['2018-10-17', 'auto', ''],
              ['2018-10-16', 'auto', ''],
              ['2018-10-15', 'auto', ''],
              ['2018-10-14', 'auto', ''],
              ['2018-10-13', 'Auto', ''],
              ['2018-10-12', 'Auto', ''],


          ]
       };

  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, -1],
        [10, 25, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    // const table = $('#datatables').DataTable();

    // $('.card .material-datatables label').addClass('form-group');
  }
}
