import { Routes } from '@angular/router';

import { VrsComponent } from './vrs/vrs.component';


export const StatisticRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'vrs',
        component: VrsComponent
    }]}
];
